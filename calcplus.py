#!/usr/bin/python3
# -*- coding: utf-8 -*-

import calcoohija
import sys

if __name__ == "__main__":

    fichero = open(sys.argv[1])
    lineas = fichero.readlines()

    for linea in lineas:
        palabra = linea.split(",")
        operador = palabra[0]
        result = palabra[1]
        for sumando in palabra[2:]:
            Texas = calcoohija.CalculadoraHija(operador, result, sumando)
            result = Texas.operar()
        print(palabra[0])
        print(result)
