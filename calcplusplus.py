#!/usr/bin/python3
# -*- coding: utf-8 -*-

import calcoohija
import sys
import csv

if __name__ == "__main__":

    with open(sys.argv[1], newline='') as fichero:

        linea = csv.reader(fichero, delimiter=",")
        for palabra in linea:
            operador = palabra[0]
            result = palabra[1]
            for sumandos in palabra[2:]:
                calcu = calcoohija.CalculadoraHija(operador, result, sumandos)
                result = calcu.operar()
            print(palabra[0])
            print(result)
