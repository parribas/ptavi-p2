#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys


class Calculadora:

    def suma(self):

        return int(self.sumando1) + int(self.sumando2)

    def resta(self):

        return int(self.sumando1) - int(self.sumando2)

    def operar(self):

        if self.operador == "suma":
            return self.suma()
        elif self.operador == "resta":
            return self.resta()
        else:
            return "Operación podrá ser suma, resta, multiplica o divide."

    def __init__(self, operador, sumando1, sumando2):

        self.operador = operador
        self.sumando1 = sumando1
        self.sumando2 = sumando2


if __name__ == "__main__":

    operador = sys.argv[2]

    try:
        sumando1 = sys.argv[1]
        sumando2 = sys.argv[3]

    except:
        sys.exit("Error: Non numerical parameters")

    Casio = Calculadora(operador, sumando1, sumando2)
    resultado = Casio.operar()
    print(resultado)
