#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def multiplicacion(self):

        return int(self.sumando1) * int(self.sumando2)

    def division(self):

        if self.sumando2 == 0:
            return "Division by zero is not allowed"
        else:
            return int(self.sumando1) / int(self.sumando2)

    def operar(self):

        if self.operador == "multiplica":
            return self.multiplicacion()
        elif self.operador == "divide":
            return self.division()
        else:
            return calcoo.Calculadora.operar(self)


if __name__ == "__main__":

    operador = sys.argv[2]

    try:
        sumando1 = int(sys.argv[1])
        sumando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    casio_junior = CalculadoraHija(operador, sumando1, sumando2)
    resultado = casio_junior.operar()
    print(resultado)
